<?php
/**
 * @package Hello_Dolly
 * @version 1.7.2
 */
/*
Plugin Name: Hello Dolly
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Matt Mullenweg
Version: 1.7.2
Author URI: http://ma.tt/
*/

function showAnnuaire(){
    global $wpdb;
    $result = $wpdb->get_results("SELECT * FROM wp_annuaire");

    $table = '<table class="center" style="margin-left: auto; 
   margin-right: auto; border: 1px solid black;">
     <tr>
     <th style="border: 1px solid black;">Id</th>
       <th style="border: 1px solid black;">Nom Entreprise</th>
       <th style="border: 1px solid black;">Localisation Entreprise</th>
       <th style="border: 1px solid black;">Prenom Contact</th>
       <th style="border: 1px solid black;">Nom Contact</th>
       <th style="border: 1px solid black;">Mail Contact</th>
     </tr>';


    foreach($result as $info) {
        $table .= '<tr>
           <td>'.$info->id.'</td>
           <td>'.$info->nom_entreprise.'</td>
           <td>'.$info->localisation_entreprise.'</td>
           <td>'.$info->prenom_contact.'</td>
           <td style=text-align:right>'.$info->nom_contact.'</td>
           <td style=text-align:right>'.$info->mail_contact.'</td>
           </tr>';
    }
    $table .= '</table>';

    return $table;
}
add_shortcode( 'annuaire', 'showAnnuaire' );