# Eval wordpress


## Pré-requis :
    MAMP

## Installation

1. Après avoir installé MAMP :

Cliquer sur le bouton WebStart.
Une fois sur le site local de MAMP, ouvrir PHPmyadmin et créer une base de données que vous pouvez nommer **wp_DB**.
Copier le contenu et lancer le script SQL qui est présent dans le répertoire **wordpress**, dans le fichier :

```
testWordpress.sql
```
ou, importer directement ce fichier dans PHPmyadmin.

2. Installer votre Wordpress :

Tout d'abord, cloner ce dossier dans le dossier Htdocs, du dossier MAMP:

```
cd Applications/MAMP/htdocs
```

3. Lancer Wordpress:

Toujours sur le site local de MAMP, cliquer sur l'onglet 'My Website'.
Une fois fait, cliquer sur votre dossier wordpress.
Votre site est disponible à cette adresse:

```
http://localhost:8888/wordpress/
```

4. Configuration Wordpress

Configurer la base de données en donnant le nom que vous avez précisez à l'étape 1, puis le mot de passe de l'administrateur.

Vous pouvez maintenant profitez de votre site internet !!!
